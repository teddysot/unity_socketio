import React from 'react'
import Unity, { UnityContext } from "react-unity-webgl";

function App() {
  const unityContext = new UnityContext({
    loaderUrl: "Build/Build.loader.js",
    dataUrl: "Build/Build.data",
    frameworkUrl: "Build/Build.framework.js",
    codeUrl: "Build/Build.wasm",
  });

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100vw', height: '100vh' }}>
      <div style={{ display: 'flex', width: '100%', height: '100%' }}>
        <Unity height="100%" width="100%" unityContext={unityContext} />
      </div>
    </div>
  );
}

export default App;
