﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using Utility;
using Scriptable;
using TMPro;

public class NetworkClient : SocketIOComponent
{
    [Header("Network Client")]

    [SerializeField] private Transform networkContainer;

    [SerializeField] private GameObject playerPrefab;

    [SerializeField] private ServerObjects serverSpawnables;

    [SerializeField] private TextMeshProUGUI _Text;

    public static string ClientID {get; private set;}

    private Dictionary<string, NetworkIdentity> serverObjects;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        _Text.text = "Network Client Start\n";
        _Text.text += $"{url}\n";
        initialize();
        setupEvents();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }

    private void initialize()
    {
        serverObjects = new Dictionary<string, NetworkIdentity>();
        _Text.text += "Initialize\n";
    }

    private void setupEvents()
    {
        _Text.text += "Setup Events\n";
        On("open",(E) => {
            Debug.Log("Connection Established");
            _Text.text += "Connection Established\n";
        });

        On("register", (E) => {
            ClientID = E.data["id"].ToString().RemoveQuotes();

            Debug.Log($"Our Client's ID ({ClientID})");
            _Text.text += $"Our Client's ID ({ClientID})";
        });

        On("spawn", (E) => {
            // Handling all spawning all players
            // Passed Data
            string id = E.data["id"].ToString().RemoveQuotes();

            GameObject go = Instantiate(playerPrefab, networkContainer);
            go.name = $"Player {id}";
            NetworkIdentity ni = go.GetComponent<NetworkIdentity>();
            ni.SetControllerID(id);
            ni.SetSocketReference(this);
            serverObjects.Add(id, ni);
        });

        On("disconnected", (E) => {
            string id = E.data["id"].ToString().RemoveQuotes();

            GameObject go = serverObjects[id].gameObject;
            Destroy(go); // Destroy from game
            serverObjects.Remove(id); // Remove from memory

            _Text.text += $"Disconnected {id}\n";
            Debug.Log($"Disconnected {id}");
        });

        On("updatePosition",(E) => {
            string id = E.data["id"].ToString().RemoveQuotes();
            float x = E.data["position"]["x"].f;
            float y = E.data["position"]["y"].f;

            NetworkIdentity ni = serverObjects[id];
            ni.transform.position = new Vector3(x,y, 0);
        });

        On("updateRotation", (E) => {
            string id = E.data["id"].ToString().RemoveQuotes();
            float tankRotation = E.data["tankRotation"].f;
            float barrelRotation = E.data["barrelRotation"].f;

            NetworkIdentity ni = serverObjects[id];
            ni.transform.localEulerAngles = new Vector3(0,0,tankRotation);
            ni.GetComponent<PlayerManager>().SetRotation(barrelRotation);
        });

        On("serverSpawn", (E) => {
            string name = E.data["name"].str;
            string id = E.data["id"].ToString().RemoveQuotes();
            float x = E.data["position"]["x"].f;
            float y = E.data["position"]["y"].f;
            Debug.Log($"Server spawned {name}");

            if(serverObjects.ContainsKey(id) == false)
            {
                ServerObjectData sod = serverSpawnables.GetObjectByName(name);
                var spawnedObject = Instantiate(sod.Prefab, networkContainer);
                spawnedObject.transform.position = new Vector3(x,y,0);
                var ni = spawnedObject.GetComponent<NetworkIdentity>();
                ni.SetControllerID(id);
                ni.SetSocketReference(this);

                // If Bullet apply direction
                if(name == "Bullet")
                {
                    float dirX = E.data["direction"]["x"].f;
                    float dirY = E.data["direction"]["y"].f;

                    float rot = Mathf.Atan2(dirY, dirX) * Mathf.Rad2Deg;
                    Vector3 currentRotation = new Vector3(0,0, rot - 90);
                    spawnedObject.transform.rotation = Quaternion.Euler(currentRotation);
                }

                serverObjects.Add(id, ni);
            }
        });

        On("serverUnspawn", (E) => {
            string id = E.data["id"].ToString().RemoveQuotes();
            NetworkIdentity ni = serverObjects[id];
            serverObjects.Remove(id);
            DestroyImmediate(ni.gameObject);
        });
    }

}
[Serializable]
public class Player
{
    public string id;
    public Position position;
}
[Serializable]
public class Position
{
    public float x;
    public float y;
}

[Serializable]
public class PlayerRotation
{
    public float tankRotation;
    public float barrelRotation;
}

[Serializable]
public class BulletData 
{
    public string id;
    public Position position;
    public Position direction;
}