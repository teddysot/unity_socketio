const io = require('socket.io')(process.env.PORT || 52300)

// Custom Classes
const Player = require('./classes/Player')
const Bullet = require('./classes/Bullet')

console.log(`Server is starting`)

const players = []
const sockets = []
const bullets = []

// Updates
setInterval(() => {
    bullets.forEach(bullet => {
        const isDestroyed = bullet.onUpdate()

        // Remove
        if (isDestroyed) {
            const index = bullets.indexOf(bullet)
            if (index > -1) {
                bullets.splice(index, 1)

                const returnData = {
                    id: bullet.id
                }

                for (const id in players) {
                    sockets[id].emit('serverUnspawn', returnData)
                }
            }
        }
        else {
            const returnData = {
                id: bullet.id,
                position: {
                    x: bullet.position.x,
                    y: bullet.position.y,
                }
            }

            for (const id in players) {
                sockets[id].emit('updatePosition', returnData)
            }
        }
    })
}, 100, 0)

io.on('connection', (socket) => {

    const player = new Player()
    const playerID = player.id

    players[playerID] = player
    sockets[playerID] = socket

    console.log(`Connected ${playerID}`);
    // Tell the client that this is our id for the server
    socket.emit('register', { id: playerID })
    socket.emit('spawn', player) // Tell myself I have spawned
    socket.broadcast.emit('spawn', player) // Tell other I have spawned

    // Tell myself about everyone else in the game
    for (const id in players) {
        if (id != playerID) {
            socket.emit('spawn', players[id])
        }
    }

    // Positional Data from Client
    socket.on('updatePosition', (data) => {
        player.position.x = data.position.x
        player.position.y = data.position.y

        socket.broadcast.emit('updatePosition', player)
    })

    socket.on('updateRotation', (data) => {
        player.tankRotation = data.tankRotation
        player.barrelRotation = data.barrelRotation

        socket.broadcast.emit('updateRotation', player)
    })

    socket.on('fireBullet', (data) => {
        const bullet = new Bullet()
        bullet.name = 'Bullet'
        bullet.position.x = data.position.x
        bullet.position.y = data.position.y
        bullet.direction.x = data.direction.x
        bullet.direction.y = data.direction.y

        bullets.push(bullet)

        const returnData = {
            name: bullet.name,
            id: bullet.id,
            position: {
                x: bullet.position.x,
                y: bullet.position.y
            },
            direction: {
                x: bullet.direction.x,
                y: bullet.direction.y
            }
        }

        socket.emit('serverSpawn', returnData)
        socket.broadcast.emit('serverSpawn', returnData)
    })

    socket.on('disconnect', () => {
        console.log(`Disconnected ${playerID}`);
        delete players[playerID]
        delete sockets[playerID]
        socket.broadcast.emit('disconnected', player)
    })
})

const interval = (func, wait, times) => {
    const interv = ((w, t) => {
        return () => {
            if (typeof t === "undefined" || t-- > 0) {
                setTimeout(interv, w)
                try {
                    func.call(null)
                }
                catch (e) {
                    t = 0;
                    throw e.toString();
                }
            }
        }
    })(wait, times)

    setTimeout(interv, wait)
}