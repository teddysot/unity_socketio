module.exports = class Vector2 {
    constructor(x = 0, y = 0) {
        this.x = x
        this.y = y
    }

    Magnitude() {
        return Math.sqrt((this.x ** 2) + (this.y ** 2))
    }

    Normalized() {
        const mag = this.Magnitude()
        return new Vector2(this.x / mag, this.y / mag)
    }

    Distance(OtherVec = Vector2) {
        const dir = new Vector2()
        dir.x = OtherVec.x - this.x
        dir.y = OtherVec.y - this.y
        return dir.Magnitude()
    }

    ConsoleOutput() {
        return `(${this.x},${this.y})`
    }
}